'use strict';

module.exports = appInfo => {
  const config = exports = {}

  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + '_1590126529262_6996'

  // 配置中间件
  config.middleware = [
    'auth',
    'handleIp',
    'printDate'
  ]
  // 中间件传参数
  config.handleIp = {
    ipArry: ['127.0.0.1']
  }
  // 中间件传入参数
  config.printDate = {
    aaa: 'aaaa'
  }

  // 配置session
  config.session = {
    key: 'SESSION_HAHA',
    maxAge: 10000, // 10s
    httpOnly: true,
    encrypt: true,
    renew: true // 延长会话有效期,每次客户端有新请求 就会重新开始设置过期时间
  }
  // 配置模版引擎
  config.view = {
    mapping: {
      '.html': 'ejs'
    }
  }

  // 自定义配置
  const otherConfig = {
    baseUrl: 'http://www.phonegap100.com/'
  }

  return {
    ...config,
    ...otherConfig
  }
}
