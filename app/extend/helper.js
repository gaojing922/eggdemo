const sd = require('silly-datetime')
module.exports = {
  // 格式化时间戳
  formatTime(time) {
    return sd.format(new Date(time * 1000), 'YYYY-MM-DD HH:mm:ss')
  }
}