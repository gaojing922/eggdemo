'use strict';

const Controller = require('egg').Controller;

class NewsController extends Controller {
  async index() {
    const { app, ctx, service } = this
    // 调用extend里面扩展的application方法
    app.foo()

    // 调用extend里面扩展的context方法
    console.log(234234234, ctx.getApi())

    // 调用extend里面扩展的helper方法 helper挂载在ctx下面
    // ctx.helper()

    // 调用extend里面扩展的request方法 request挂载在ctx下面
    // ctx.getReq()

    const data = await service.news.getNewsList()
    await ctx.render('news', {
      data
    })
  }

  // 详情
  async newsDetail() {
    const { ctx, service } = this
    const { aid } = ctx.query
    const data = await service.news.getNewsDetail(aid)
    await ctx.render('newsDetail', {
      data: data[0]
    })
  }
}

module.exports = NewsController
