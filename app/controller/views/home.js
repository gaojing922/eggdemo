'use strict';
const Controller = require('egg').Controller

class HomeController extends Controller {
  async index() {
    const { ctx } = this

    // 获取cookie
    // const cookieUserinfo = ctx.cookies.get('cookieUserinfo', {
    //   encrypt: true // 获取的时候要解密
    // })
    // 删除cookie
    // ctx.cookies.set('userinfo', null)
    // 路由跳转 ctx.redirect('/)

    // 获取session
    const sessionUserinfo = ctx.session.sessionUserinfo
    console.log(23423423, sessionUserinfo)
    await ctx.render('index', {
      list: ['1111', '2222', '3333'],
      title: '首页',
      // cookieUserinfo: JSON.parse(cookieUserinfo),
      sessionUserinfo
    })
  }

  async login() {
    const { ctx } = this
    // 当用户访问页面的时候生成一个秘钥
    await ctx.render('login')
  }

  async error() {
    this.ctx.body = '页面不存在'
  }
}

module.exports = HomeController
