'use strict'
const Controller = require('egg').Controller

class HomeController extends Controller {
  async login() {
    // 获取post提交数据
    const { username, password } = this.ctx.request.body

    // 设置cookie 在eggjs里面只有加密码之后 才能设置中文cookie
    // this.ctx.cookies.set('cookieUserinfo', JSON.stringify({username, password}), {
    //   maxAge: 1000 * 3600 * 24,
    //   httpOnly: true,
    //   signed: true, // 对cookie进行签名, 防止用户修改cookie
    //   encrypt: true // 对cookie进行加密码，获取的时候要解密
    // })

    // 设置session egg内置了egg-session插件 session配置在config.default.js里面配置
    this.ctx.session.sessionUserinfo = {
      username,
      password
    }
    console.log('设置成功', this.ctx.session.sessionUserinfo)
    this.ctx.body = {
      username,
      password
    }
  }
}

module.exports = HomeController
