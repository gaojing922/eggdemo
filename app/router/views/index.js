// 所有页面路由
module.exports = app => {
  const { router, controller } = app
  router.get('/', controller.views.home.index)
  router.get('/login', controller.views.home.login)
  router.get('/news', controller.views.news.index)
  router.get('/newsDetail', controller.views.news.newsDetail)
  router.get('*', controller.views.home.error)
  // router.post('/handleLogin', controller.home.handleLogin)
}