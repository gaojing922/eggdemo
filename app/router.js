'use strict';

/**
 * @param {Egg.Application} app - egg application
 */
module.exports = app => {
  // 页面路由
  require('./router/views')(app)
  // api
  require('./router/api/login')(app)
}
