// options: 中间件的配置项，框架会将 app.config[${middlewareName}] 传递进来。
// app: 当前应用 Application 的实例。
// 使用中间件还得配置  在config.default.js middleware里面配置
module.exports = (options, app) => {
  console.log('app', app)
  console.log('在config.default.js里面配置的参数', options)
  // 返回一个异步方法
  return async function printDate(ctx, next) {
    console.log(898989, new Date())
    await next()
  }
}