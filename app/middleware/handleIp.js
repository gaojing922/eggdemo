module.exports = (options, app) => {
  return async function handleIp(ctx, next) {
    // 需要屏蔽的ip
    console.log(options.ipArry)
    // 获取客户端的IP
    const clientIp = ctx.request.ip
    console.log('ip为:' + clientIp)
    await next()
  }
}