'use strict';

const Service = require('egg').Service;

class NewsService extends Service {

  // 获取新闻列表
  async getNewsList() {
    const { config, ctx } = this
    const url = config.baseUrl + 'appapi.php?a=getPortalList&catid=20&page=1'
    // egg提供 curl方法 可以获取远程数据
    const res = await ctx.curl(url)
    return JSON.parse(res.data).result || []
  }

  // 获取新闻详情
  async getNewsDetail(aid) {
    const { config, ctx } = this
    const url = config.baseUrl + 'appapi.php?a=getPortalArticle&aid=' + aid
    const res = await ctx.curl(url)
    return JSON.parse(res.data).result || []
  }

}

module.exports = NewsService;
